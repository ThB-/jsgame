

function Draw() {
    var c = document.getElementById("rectangle");
    var ctx = c.getContext("2d");
    ctx.beginPath();
    ctx.rect(20, 20, 150, 100);
    ctx.stroke();
}


function Write() {
    var canvas = document.getElementById("ecriture");
    var ctx = canvas.getContext("2d");
    ctx.font = "30px Arial";
    ctx.fillText("Play!", 10, 50);
    canvas.addEventListener('mousemove', e => {
        ctx.fillStyle = "red";
    }
    );
    canvas.addEventListener('mouseout', e => {
        ctx.fillStyle = "black";
    }
    );
}

function bloc_Principal() {
    var blocP = document.getElementById("bloc");
    var contenu = blocP.getContext("2d");
    contenu.beginPath();
    contenu.rect(50, 50, 200, 100);
    contenu.stroke();
}


setInterval(Draw, 5);
setInterval(Write, 5);
setInterval(bloc_Principal, 5);
